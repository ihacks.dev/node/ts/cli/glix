glix
====

A custom domain manager for Glix.

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/glix.svg)](https://npmjs.org/package/glix)
[![Downloads/week](https://img.shields.io/npm/dw/glix.svg)](https://npmjs.org/package/glix)
[![License](https://img.shields.io/npm/l/glix.svg)](https://github.com/https://gitlab.com/ihack2712/https://gitlab.com/ihacks.dev/node/ts/cli/glix/blob/master/package.json)

**Installation**

Using npm:

```sh
npm i -g glix
```

Or with yarn:

```sh
yarn global add glix
```

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g glix
$ glix COMMAND
running command...
$ glix (-v|--version|version)
glix/0.1.2 darwin-x64 node-v14.7.0
$ glix --help [COMMAND]
USAGE
  $ glix COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`glix help [COMMAND]`](#glix-help-command)
* [`glix login`](#glix-login)
* [`glix logout`](#glix-logout)
* [`glix register`](#glix-register)
* [`glix whois`](#glix-whois)

## `glix help [COMMAND]`

display help for glix

```
USAGE
  $ glix help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.2.0/src/commands/help.ts)_

## `glix login`

Sign in to your glix account.

```
USAGE
  $ glix login

ALIASES
  $ glix signin
  $ glix si
```

_See code: [src/commands/login.ts](https://gitlab.com/ihack2712/https%3A//gitlab.com/ihacks.dev/node/ts/cli/glix/blob/v0.1.2/src/commands/login.ts)_

## `glix logout`

Log out of an account.

```
USAGE
  $ glix logout

ALIASES
  $ glix signout
  $ glix so
```

_See code: [src/commands/logout.ts](https://gitlab.com/ihack2712/https%3A//gitlab.com/ihacks.dev/node/ts/cli/glix/blob/v0.1.2/src/commands/logout.ts)_

## `glix register`

Create a new Glix account.

```
USAGE
  $ glix register

ALIASES
  $ glix signup
  $ glix su
```

_See code: [src/commands/register.ts](https://gitlab.com/ihack2712/https%3A//gitlab.com/ihacks.dev/node/ts/cli/glix/blob/v0.1.2/src/commands/register.ts)_

## `glix whois`

Get information about the current logged in user.

```
USAGE
  $ glix whois

ALIASES
  $ glix who
  $ glix w
```

_See code: [src/commands/whois.ts](https://gitlab.com/ihack2712/https%3A//gitlab.com/ihacks.dev/node/ts/cli/glix/blob/v0.1.2/src/commands/whois.ts)_
<!-- commandsstop -->
