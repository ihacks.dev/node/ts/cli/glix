// Imports
import { Command } from "@oclif/command";
import {
	authenticated,
	logout,
	login,
	verify,
	me
} from "../api";
import {
	prompt,
	email,
	password
} from "../prompts";
import * as ora from "ora";

export default class Login extends Command
{
	
	public static description = "Sign in to your glix account.";
	public static aliases = [ "signin", "si" ];
	
	public async run ()
	{
		if (await authenticated())
		{
			const a = await prompt([
				{
					name: "logout",
					type: "confirm",
					message: "You must be logged out to perform this action, logout?"
				}
			]);
			if (a.logout) await logout();
			else this.exit(0);
		}
		const answers = await prompt([ email, password ]);
		const spinner = ora("Logging you in!").start();
		let mustVerify;
		try
		{
			[,, mustVerify ] = await login(answers as any);
			spinner.succeed("Fetched tokens!");
		} catch (error)
		{
			spinner.fail(error.message);
			this.exit(1);
		}
		
		if (mustVerify)
		{
			const code = (await prompt([
				{
					name: "code",
					type: "input",
					validate: s => s.length === 6 && /^[0-9]+$/g.test(s) ? true : "Must be a 6 digit verification code."
				}
			])).code;
			const spinner = ora("Verifying account...").start();
			try
			{
				const { displayName } = await verify(code);
				spinner.succeed("Welcome " + displayName + "! Your account has now been verified! You're now logged in.");
				return;
			} catch (error)
			{
				spinner.fail(error.message);
				this.exit(1);
			}
		}
		
		const { displayName } = await me();
		this.log("Welcome back " + displayName + "! You're now logged in.");
	}
	
}
