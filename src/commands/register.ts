// Imports
import { Command } from "@oclif/command";
import { register } from "../api";
import {
	prompt,
	displayName,
	email,
	passwordV
} from "../prompts";
import * as ora from "ora";

export default class Register extends Command
{
	
	public static description = "Create a new Glix account.";
	public static aliases = [ "signup", "su" ];
	
	public async run ()
	{
		const answers = await prompt([ email, displayName, passwordV ]);
		const spinner = ora("Creating Glix account.").start();
		try
		{
			const id = await register(answers as any);
			spinner.succeed("Successfully created Glix account, please verify your account...");
		} catch (error)
		{
			spinner.fail(error.message);
			this.exit(1);
		}
	}
	
}
