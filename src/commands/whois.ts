// Imports
import { Command } from "@oclif/command";
import { me } from "../api";

export default class Whois extends Command
{
	
	public static description = "Get information about the current logged in user.";
	public static aliases = [ "who", "w" ];
	
	public async run ()
	{
		const info = await me();
		console.log(info);
	}
	
}
