// Imports
import { Command } from "@oclif/command";
import { logout } from "../api";

export default class Logout extends Command
{
	
	public static description = "Log out of an account.";
	public static aliases = [ "signout", "so" ];
	
	public async run ()
	{
		await logout();
		console.log("Bye 👋");
	}
	
}
