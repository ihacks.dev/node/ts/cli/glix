import { Question} from "inquirer";
import * as joi from "joi";

export { prompt } from "inquirer";

export const displayName: Question = {
	name: "displayName",
	validate: str => str.length > 1 && str.length < 33 && /^[a-z0-9_\-]+$/gi.test(str) ? true : "Must be between 2-32 characters and only contain the following characters a-zA-Z0-9_-",
	type: "input"
};
export const email: Question = {
	name: "email",
	validate: str => {
		const r = joi.string().email().required().validate(str);
		return !(r.error || r.errors) ? true : "Must be a valid email address."
	},
	type: "input"
};

export const passwordV: Question & { mask: string } = {
	name: "password",
	validate: s => s.length > 7 && /[a-zæøå]/g.test(s) && /[A-ZÆØÅ]/g.test(s) && /[0-9]/g.test(s) && /[^a-zA-Z0-9æøåÆØÅ]/g.test(s) ? true : "Password must at least be 8 characters with 1 lowercase, 1 uppercase, 1 number and 1 symbol.",
	type: "password",
	mask: "*"
};

export const password: Question & { mask: string } = {
	name: "password",
	type: "password",
	mask: "*"
};
