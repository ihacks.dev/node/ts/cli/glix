// Imports
import {
	APIError,
	clear, set, authenticated,
	get,
	post
} from "./requests";
export { authenticated } from "./requests";

export type UserObj = {
	id: string,
	email: string,
	displayName: string,
	admin: boolean,
	beta: boolean,
	verified: boolean
};

const notAuthenticatedError = new APIError(401, "Missing authentication token!");
const authenticatedError = new APIError(401, "Must be unauthenticated to perform this action!");

const authreq = async () => {
	if (!await authenticated())
		throw notAuthenticatedError;
};
const unauth = async () => {
	if (await authenticated())
		throw authenticatedError;
};

export const logout = async () => await clear();

export const register = async ({ email, displayName, password }: { email: string, displayName: string, password: string }): Promise<string> => {
	await unauth();
	return await post("/users/create", { email, displayName, password });
}
export const login = async ({ email, password }: { email: string, password: string }): Promise<string> => {
	await unauth();
	return await post("/users/login", { email, password });
}
export const me = async (): Promise<UserObj> => {
	await authreq();
	return await get("/users/me");
};

export const verify = async (code: string): Promise<UserObj> => {
	await authreq();
	return await get("/users/me/" + code);
};
