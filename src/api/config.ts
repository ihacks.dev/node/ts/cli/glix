// Imports
import {
	existsSync,
	readFile as readAsync,
	writeFile as writeAsync
} from "fs";
import { join } from "path";
import { homedir } from "os";

const readFile = (path: string): Promise<Buffer> => new Promise<Buffer>(async (resolve, reject) => {
	readAsync(path, (err?: any, data?: Buffer) => {
		if (err) return reject(err);
		resolve(data!);
	})
});

const writeFile = (path: string, content: string): Promise<void> => new Promise<void>(async (resolve, reject) => {
	writeAsync(path, content, (err?: any) => {
		if (err) return reject(err);
		resolve();
	})
});

type OptionalConfig = {
	token?: string,
	refreshToken?: string
};

let cachedConfig: OptionalConfig;

export const _configPath: string = join(homedir(), ".glix.json");
export const hasConfig = (): boolean => existsSync(_configPath);
export const readConfig = async (): Promise<any> => (await readFile(_configPath)).toString();
export const getConfig = async (): Promise<{
	token?: string,
	refreshToken?: string
}> => {
	if (cachedConfig) return cachedConfig;
	let conf: OptionalConfig;
	try
	{
		const config = await readConfig();
		const json = JSON.parse(config);
		if (!json.token || !json.refreshToken) conf = {};
		else conf = json;
	} catch (error)
	{
		return conf = {};
	}
	cachedConfig = conf;
	return await saveConfig(cachedConfig);
};
export const saveConfig = async (config: OptionalConfig) => {
	for (let key in config)
	{
		if (key === "token" || key === "refreshToken") continue;
		(cachedConfig as any)[key] = undefined;
	}
	if (!config) config = {};
	if (!config.token || !config.refreshToken)
	{
		config.token = undefined;
		config.refreshToken = undefined;
	}
	cachedConfig = config;
	await writeFile(_configPath, JSON.stringify({ token: config.token, refreshToken: config.refreshToken }));
	return config;
}
