// Imports
import fetch, { Response } from "node-fetch"
import { getConfig, saveConfig } from "./config";
import { decode } from "jsonwebtoken";

type InferPromise <A> = A extends Promise<infer X> ? X : never;

export class APIError extends Error
{
	public code: number;
	public json ()
	{
		return {
			code: this.code,
			message: this.message
		};
	}
	public constructor (code: number, message: string)
	{
		super(code + " - " + message);
		this.code = code;
	}
}

export default class Requests
{
	
	#tokenExpiration?: number;
	#token?: string;
	#refreshToken?: string;
	
	public authenticated (): boolean
	{
		return !!this.#token && !!this.#refreshToken;
	}
	
	public async load ()
	{
		const config = await getConfig();
		if (config.token)
		{
			this.#token = config.token;
			this.#refreshToken = config.refreshToken;
			const jwt = decode(this.#token) as any;
			this.#tokenExpiration = jwt.exp * 1000;
		}
		return this;
	}
	
	public async request (url: string, method: "GET" | "PUT" | "POST" | "PATCH" | "DELETE" = "GET", data?: any): Promise<any>
	{
		let response: Response;
		
		try
		{
			response = await fetch("https://api.glix.ihacks.dev" + url, {
				method,
				...(() => typeof data !== "undefined" && method !== "GET" ? { body: JSON.stringify(data) } : {})(),
				headers: {
					"Content-Type": "application/json",
					Accepts: "application/json",
					...(() => typeof this.#token === "string" ? { Authorization: this.#token, "X-Refresh-Token": this.#refreshToken } : {})(),
				} as any
			});
		} catch (error)
		{
			throw error;
		}
		
		if (response.headers.has("x-new-token") && response.headers.has("x-new-refresh-token"))
			await this.setTokens(response.headers.get("x-new-token")!, response.headers.get("x-new-refresh-token")!);
		let json;
		try
		{
			json = await response.json();
		} catch (error)
		{
			console.log(await response.text());
			throw error;
		}
		if (typeof json === "object" && json !== null && typeof json.code === "number" && typeof json.message === "string")
		{
			const error = new APIError(json.code, json.message);
			if (error.code === 401 && json.message === "Must be unauthenticated to perform this action!") await this.clearTokens();
			throw error;
		}
		return json;
	}
	
	public async get (url: string): Promise<any> { return await this.request(url, "GET"); }
	public async post (url: string, data?: any): Promise<any> { return await this.request(url, "POST", data); }
	public async delete (url: string, data?: any): Promise<any> { return await this.request(url, "DELETE", data); }
	public async put (url: string, data?: any): Promise<any> { return await this.request(url, "PUT", data); }
	public async patch (url: string, data?: any): Promise<any> { return await this.request(url, "PATCH", data); }
	
	public async setTokens (token: string, refreshToken: string)
	{
		this.#token = token;
		this.#refreshToken = refreshToken;
		const jwt = decode(this.#token) as any;
		this.#tokenExpiration = jwt.exp * 1000;
		const config = await getConfig();
		config.token = this.#token;
		config.refreshToken = this.#refreshToken;
		await saveConfig(config);
	}
	
	public async clearTokens ()
	{
		this.#token = undefined;
		this.#refreshToken = undefined;
		this.#tokenExpiration = undefined;
		const config = await getConfig();
		config.token = undefined;
		config.refreshToken = undefined;
		await saveConfig(config);
	}
	
}

let api: Requests;

export const init = async () =>
{
	if (!api)
	{
		api = new Requests();
		await api.load();
	}
	return api;
};

export const get = async (url: string) =>
{
	const api = await init();
	return await api.get(url);
}

export const post = async (url: string, data?: any) => await (await init()).post(url, data);
export const patch = async (url: string, data?: any) => await (await init()).patch(url, data);
export const put = async (url: string, data?: any) => await (await init()).put(url, data);
export const remove = async (url: string, data?: any) => await (await init()).delete(url, data);
export const clear = async () => await (await init()).clearTokens();
export const set = async (token: string, refreshToken: string) => await (await init()).setTokens(token, refreshToken);
export const authenticated = async () => (await init()).authenticated();
